/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/
#include "TrigFTKSim/FTKRoadFinderAlgo.h"
#include "TrigFTKSim/FTKDumpCondAlgo.h"
#include "TrigFTKSim/FTKTrackFitterAlgo.h"
#include "TrigFTKSim/FTKMergerAlgo.h"
#include "TrigFTKSim/FTK_SGHitInput.h"
#include "TrigFTKSim/FTK_SGRoadOutput.h"
#include "TrigFTKSim/FTK_SGTrackOutput.h"
#include "TrigFTKSim/FTK_RoadMarketTool.h"
#include "TrigFTKSim/FTKDetectorTool.h"

DECLARE_COMPONENT( FTKRoadFinderAlgo )
DECLARE_COMPONENT( FTKDumpCondAlgo )
DECLARE_COMPONENT( FTKTrackFitterAlgo )
DECLARE_COMPONENT( FTK_SGHitInput )
DECLARE_COMPONENT( FTK_SGRoadOutput )
DECLARE_COMPONENT( FTK_SGTrackOutput )
DECLARE_COMPONENT( FTK_RoadMarketTool )
DECLARE_COMPONENT( FTKDetectorTool )
DECLARE_COMPONENT( FTKMergerAlgo )

